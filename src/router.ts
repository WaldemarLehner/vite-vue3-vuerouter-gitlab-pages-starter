import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import LandingPage from "./pages/LandingPage.vue"
import ProjectSummaryPage from "./pages/ProjectSummaryPage.vue"
import ProjectPage from "./pages/ProjectPage.vue"
import NotFoundPage from "./pages/NotFoundPage.vue"




const routes: RouteRecordRaw[] = [
    {
        path: "/",
        component: LandingPage,
        name: "Landing"
    },
    {
        path:"/projects",
        component: ProjectSummaryPage,
        name: "Projects"
    },
    {
        path: "/projects/:id",
        component: ProjectPage,
    },
    {
        path: "/404",
        component: NotFoundPage,
        name: "NotFound"
    },
    {
        path: "/:pathMatch(.*)",
        redirect: {name: "NotFound"},
    }
];

console.log("Base URL is "+import.meta.env.BASE_URL);

export default createRouter({
    routes,
    history: createWebHistory(import.meta.env.BASE_URL),
})