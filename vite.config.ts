import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

function getBaseUrl() {
  const isCI = process.env.GITLAB_BUILD == "1";
  if(!isCI) {
    return "";
  }
  const url = new URL(process.env.CI_PAGES_URL);
  return url.pathname+url.search;
}



// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue()
  ],
  base: getBaseUrl(),
  build: {
    assetsInlineLimit: 0 // Disabled just to be able to test that fetch() gets the right URL.
  }
})
